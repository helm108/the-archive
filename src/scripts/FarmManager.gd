extends Node

onready var world: Node2D = get_node('/root/' + Game.currentScene + '/' + Game.currentWorld)
var plantScene: PackedScene = preload('res://src/components/Plant.tscn')
var ground: TileMap

var tile_highlight: Sprite
var tile_name: String
var tile_pos: Vector2
var plant
var soil_tile_id
var dry_soil_tile_id

func _ready():
	ground = world.get_node('Farmable')
	soil_tile_id = ground.tile_set.find_tile_by_name('Soil')
	dry_soil_tile_id = ground.tile_set.find_tile_by_name('Dry Soil')
	
	var _e1 = Events.connect('select_tool', self, '_force_update')
	var _e2 = Events.connect('focused_tile', self, '_focused_tile_updated')
	var _e3 = Events.connect('day_process', self, '_update_farm')

func _input(_event):
	if Input.is_action_pressed('interact'):
		if Game.currentTool == Game.Tools.HANDS && plant && plant.harvestable:
			_harvest_plant(plant)
		elif Game.currentTool == Game.Tools.SHOVEL && tile_name == 'Farmable':
			ground.set_cellv(tile_pos, soil_tile_id)
		elif Game.currentTool == Game.Tools.WATERING_CAN && tile_name == 'Dry Soil':
			_water_plant(tile_pos)
		elif Game.currentTool == Game.Tools.SEEDS && tile_name == 'Soil':
			_plant_seed(tile_pos)

		_focused_tile_updated(tile_pos)

func _force_update(_current_tool):
	_focused_tile_updated(tile_pos)	

func _focused_tile_updated(_tile_pos: Vector2):
	tile_pos = _tile_pos
	tile_name = _identify_tile(tile_pos)
	plant = Game.get_plant(tile_pos)
	
	if Game.currentTool == Game.Tools.HANDS && plant && plant.harvestable:
		_position_tile_highlight()
	elif Game.currentTool == Game.Tools.SHOVEL && tile_name == 'Farmable':
		_position_tile_highlight()
	elif Game.currentTool == Game.Tools.WATERING_CAN && tile_name == 'Dry Soil':
		_position_tile_highlight()
	elif Game.currentTool == Game.Tools.SEEDS && tile_name == 'Soil' && !plant:
		_position_tile_highlight()
	else:
		$TileHighlight.visible = false

func _position_tile_highlight():
	$TileHighlight.visible = true
	$TileHighlight.global_position = ground.to_global(ground.map_to_world(tile_pos))

func _identify_tile(pos: Vector2):
	var tile_id = ground.get_cellv(pos)
	if tile_id == -1:
		return ''
	return ground.tile_set.tile_get_name(tile_id)
	
func _plant_seed(_tile_pos: Vector2):
	if Game.seeds == 0:
		return
	
	var plant_name = Game.get_plant_name(_tile_pos)
	
	if Game.plants.has(plant_name):
		return

	Game.add_seeds(-1)
	
	var scene = plantScene.instance()
	
	scene.global_position = ground.map_to_world(tile_pos)
	world.add_child(scene)
		
	Game.plants[plant_name] = {
		'scene': scene,
		'plant_name': plant_name,
		'position': _tile_pos,
		'age': 0,
		'harvest_age': 3,
		'hp': 2,
		'quality': 0,
		'harvestable': false,
		'watered': true,
	}

func _water_plant(_tile_pos: Vector2):
	# update tile
	ground.set_cellv(tile_pos, soil_tile_id)
	
	# update plant
	var plant_name = "{x}-{y}".format({"x": _tile_pos.x, "y": _tile_pos.y})
	if Game.plants.has(plant_name):
		Game.plants[plant_name].watered = true
		
func _harvest_plant(_plant):
	Game.add_money(Game.carrot_value)
	Game.delete_plant(_plant.plant_name)

func _update_farm():
	_update_plants()
	_update_ground()

func _update_plants():
	var plants_to_remove: = []
	for key in Game.plants:
		var _plant = Game.plants[key]
		
		if !_plant.watered:
			_plant.hp -= 1
			
		if _plant.hp == 0:
			plants_to_remove.append(key)
			continue
		
		_plant.age += 1
		if _plant.age == _plant.harvest_age:
			_plant.harvestable = true
			
		var anim: AnimatedSprite = _plant.scene.get_node('Carrot')
				
		anim.frame += 1 if anim.frame < _plant.harvest_age else _plant.harvest_age
		
		_plant.watered = false
	
	for key in plants_to_remove:
		Game.delete_plant(key)

func _update_ground():
	var soil_cells: = ground.get_used_cells_by_id(soil_tile_id)
	for cell in soil_cells:
		ground.set_cellv(cell, dry_soil_tile_id)
