extends Node

var allow_input: = true;

func _ready():
	Events.connect("allow_input", self, '_handle_allow_input')

func _handle_allow_input(_allow_input):
	allow_input = _allow_input;

func _input(_event):
	if !allow_input:
		return
		
	if Input.is_action_pressed('select_item_1'):
		Events.emit_signal('select_tool', Game.Tools.HANDS)
		
	if Input.is_action_pressed('select_item_2'):
		Events.emit_signal('select_tool', Game.Tools.SHOVEL)
		
	if Input.is_action_pressed('select_item_3'):
		Events.emit_signal('select_tool', Game.Tools.WATERING_CAN)
		
	if Input.is_action_pressed('select_item_4'):
		Events.emit_signal('select_tool', Game.Tools.SEEDS)
