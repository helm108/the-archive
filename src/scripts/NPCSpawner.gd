extends Node2D

var npc_scene: PackedScene = load('res://src/components/characters/NPC.tscn')

var npc_definitions = [
	{
		'id': 'janet',
		'firstname': 'Janet',
		'lastname': 'Parker',
		'reputation': 0,
	},
	{
		'id': 'steven',
		'firstname': 'Steven',
		'lastname': 'Plume',
		'reputation': 0,
	},
	{
		'id': 'finlight',
		'firstname': 'Finlight',
		'lastname': 'Eldersson',
		'reputation': 0,
	},
	{
		'id': 'dad',
		'firstname': 'Dad',
		'lastname': '',
		'reputation': 0,
	}
]

var npcs: Array = []

func _tile_to_px(tile):
	return tile * 32

func _ready():
	Events.connect('day_process', self, '_set_npcs')
	Events.connect('introduction_ended', self, '_remove_dad')
	
	for definition in npc_definitions:
		Game.npcs[definition.id] = definition
	_set_npcs()
	
func _set_npcs():
	remove_npcs()
	add_npc('finlight', 25, 5, 'stand_south')
	
	# Spawn different NPCs based on the day or something
	match Game.day:
		1:
			add_npc('dad', 18, 4, 'stand_west')
			add_npc('janet', 16, 8, 'stand_west')
			add_npc('steven', 18, 8, 'stand_south')
		2:
			add_npc('janet', 16, 8, 'stand_east')
		4:
			add_npc('steven', 11, 12, 'stand_south')
		6:
			add_npc('steven', 11, 12, 'stand_south')
		8:
			add_npc('dad', 7, 6, 'stand_east')
			add_npc('janet', 16, 8, 'stand_north')
		10:
			add_npc('steven', 11, 12, 'stand_south')
		14:
			add_npc('steven', 11, 12, 'stand_south')
		16:
			add_npc('janet', 16, 8, 'stand_south')
		18:
			add_npc('steven', 11, 12, 'stand_south')
			add_npc('janet', 16, 10, 'stand_west')

# This should be a function that removes a specific NPC, but it isn't.
func _remove_dad():
	# Removes all NPCs and puts Finlight back because he's important
	remove_npcs()
	# Just copy day one here.
	add_npc('finlight', 25, 5, 'stand_south')
	add_npc('janet', 16, 8, 'stand_south')
	add_npc('steven', 18, 8, 'stand_south')

func add_npc(id, x, y, animation: String = 'stand_west'):
	x = _tile_to_px(x)
	y = _tile_to_px(y)
	
	var npc_definition = Game.npcs[id]
	var npc = npc_scene.instance()
	npc.position = Vector2(x, y)
	npc.npc_definition = npc_definition
	
	var animated_sprite: AnimatedSprite = npc.get_node('AnimatedSprite')
	animated_sprite.frames = load('res://src/assets/characters/' + npc_definition.id + ".tres")
	animated_sprite.animation = animation
	
	npcs.append(npc)
	
	get_parent().call_deferred('add_child', npc)
	
	return npc

func remove_npcs():
	# New NPCs are added to `npcs` before the loop is complete.
	# This was solved by cloning `npcs`.
	var _npcs = npcs.duplicate()
	# Clear the original array for new NPCs.
	npcs = []
	# Queue old NPCs for deleting.
	for npc in _npcs:
		var _npc: Node2D = npc
		_npc.queue_free()
