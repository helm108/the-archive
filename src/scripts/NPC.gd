extends KinematicBody2D

var npc_definition: = {}

func _ready():
	z_index = int(position.y)

func _on_InteractableArea_player_interacted_with():
	Events.emit_signal("npc_interacted_with", npc_definition)
