extends ColorRect

onready var _anim_player := $AnimationPlayer

func _ready():
	visible = true
	
	Events.connect("day_end", self, "_fade_out")
	Events.connect("day_start", self, "_fade_in")
	
func _fade_in():
	_anim_player.play("FadeIn")
	
func _fade_out():
	_anim_player.play("FadeOut")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == 'FadeOut':
		Events.emit_signal("day_process")
	elif anim_name == 'FadeIn':
		pass
