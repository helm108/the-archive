extends Label

func _ready():
	var _error: = Events.connect('seed_count_update', self, '_update')
	Game.add_seeds(2)

func _update(seeds_count):
	text = str('Seeds: ', seeds_count)
