extends Label

func _ready():
	var _error: = Events.connect('money_count_update', self, '_update')

func _update(money):
	text = str('£', money)
