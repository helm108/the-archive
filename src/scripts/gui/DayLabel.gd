extends Label

func _ready():
	var _error: = Events.connect('day_start', self, '_day_start')

func _day_start():
	text = str('Day ', Game.day)
