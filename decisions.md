# Decision Document

In this document I will record decisions that I made.

## Tiled vs Godot
I experimented with using Tiled to create maps and then import them in to Godot. I ended up choosing to stick with 
Godot.

Problems:
- The Godot plugin for importing Tiled .tmx files has been abandoned
- It still works but required inserting some code from PRs into it manually
- You cannot mark tiles as collision tiles like you can in Godot, you must manually draw collision shapes over the map 
- to have Godot generate collision nodes for you

It's usable, but the hassle wasn't worth it in the end. The map editor in Godot isn't quite as powerful, you can't 
automatically merge autotiles for example, but you can mark tiles as collision areas which I found more useful.

## Detecting What Is In Front of You
The player needs to be able to interact with NPCs and tiles. My initial solution for this is to have an Area2D floating 
a tile's width away in front of the player. Its collisions with other things can be recorded, and when the user presses 
the Interact key the code can see if the HitDetector shape is colliding with anything and then work out what to do with 
it. 

## Tried to make an NPC spawner
You can assign a tileset to an AnimatedFrame but you can't save the actual animations and the frames chosen for them. 
You would have to define all of that programmatically and that's more effort than I am interested in for this project.
Solution: Just make each Character by hand.
Idea: Find out how people would go about reducing effort of making 20 characters. What if I need to add a node to all 
of them?

### UPDATE - solved
I revisited this problem after Kate mentioned it and discovered that the .tres file created by attaching a PNG to an 
AnimatedSprite and creating frames actually contains the frame data. Obvious in hindsight.

This means that I can load an NPC scene, load a .tres file as a SpriteFrames object and set that as the `frames` 
property on the AnimatedSprite object.

The NPC spawner will thus instantiate an NPC and based on the name of the NPC load the appropriate .tres file.

## schedule.json
I decided to define the NPC schedule for each day in a JSON file. This will be loaded into a Singleton that determines
who to spawn on each given morning and which conversations they will trigger.

## Day Cycle and Global Game State
2021-08-01
The game will have a day cycle and this will dictate which NPCs appear and which conversations you will have with them.
I could dictate that each NPC has one conversation and leaves, but allowing for multiple seems easy enough. But that's 
more writing effort and we don't really expect anyone to stick around the farm after talking so maybe I just won't 
bother.

I thought I'd need a global game state for knowing what day it is, but that might not be the case. So far only the 
Conversation Manager needs to know what day it is. I can see this changing though so it might be worth going straight 
for the global state, so long as I use it sparingly.

2021-08-08
Decided to go for a global state anyway. It just makes things easier. It probably makes things easier for save/load 
stuff too but I'm not going to worry about that for this game.

2021-10-24
I want the game to fade to black, build the next day, and then fade back in with everyone in the right place. This means
working out how to very explicitly make sure that all work is done before firing day_start.

I have this appearing to work while not properly working. The day end trigger emits day_end which triggers the fade out.
When the fade out is complete it emits day_process which updates the farm and increments the day counter. Game.gd emits
day_start after incrementing the day which triggers the fade in animation but the FarmManager updates the farm after
this has happened. I am not sure how you emit a signal and then wait for all listeners to complete.

This will do for this game but something more complex would need something that actually fires a signal when all
processing is actually complete. This would probably not want to rely on signals, or would need to manually track when
signal-triggered tasks are completed via returning completion signals?

## Conversation Manager
2021-08-01
The CM currently just contains switch statements as a proof of concept. This will get out of hand quite fast and I'll 
likely just use a JSON file for the future but this is good enough for now. 

2021-08-14
Added day 2 conversations. This can be improved later but it'll do for now. If I need dynamic conversation picking (e.g.
X days after Y event) then that's a whole big thing but I'll leave that for later.

2021-09-19
I could just have it play `'janet-day${day}';` and name each conversation with the day number on the end. This would
make general day-based conversations really easy to do, and make it difficult to do one-off stuff, but I could have a
check for specific conversations first, fall back to `${id}-day${day}`.

Yep that totally worked.

2021-10-15
The current system only really allows one conversation to happen per day. This is acceptable. The only dynamic bit of
conversation really is the player offering an NPC a ticket to the gig. This would be triggered by the rep with the NPC
reaching the threshold and could be handled in the override section of ConversationManager.

2021-10-30
It kind of feels like an NPC should be spawned with what interacting with them should do?

## Player Input
2021-08-14
I currently have _input functions all over the place. I feel like it would be better to have an InputManager on the 
player that fires events to relevant things instead of putting input calls all over the place. 

2021-08-16
Started implementing this but then realised that if I'm just firing events that have to be hooked in to elsewhere then
I don't think I'm really improving anything, just adding an extra step for no real reason. I might realise I'm wrong
later but if so I'll just fix it then, or not and write down the lesson learned.

2021-08-18
Made an InputManager! Started working on selectable tools and realised there wasn't an existing file that it made sense
to do that in. An InputManager seemed like a great place to handle that, as it needs to be dealt with by the UI and by
player code. Not decided on moving existing stuff to it though, will see.

2021-08-29
Now that I have input events everywhere it feels weird having files handling their specific logic and also input? But
again I'd just be handling the same logic but from an event instead of input. Not sure why I find this weird. Maybe the
GDQuest tutorials will teach me the correct way.

## Farmable Ground
I need to be able to mark an area as farmable. I don't seem to be able to assign properties to individual tilemap tiles
so I need to be able to define areas that are farmable. If I allow any grass tile to be farmed then I can't prevent the
player from turning grass under a fence into dirt.

Options
1. Define a list of modifiable tiles in a Dictionary
2. Overlay the ground with another TileMap
3. Overlay the ground with an Area2D

I went with option three. HitDetector can determine if it's in a Farmable area by collision with the Area2D and modify 
the existing Ground TileMap.

In the process of implementing option three I learned that you can get the name of a tile. So I could duplicate the 
Grass set and name it Farmable and use THAT to determine if it can be farmed.

Turns out that that works really well. Dealing with duplicate tile sets in the editor seemed impossible until I noticed
the arrows for swapping between sets and now I can just paint the right tiles to mark farmable or not.

Godot's inability to merge different tilesets means I had to fill the edges in by hand which is a shame. I remember that
there is a way of marking different tilesets as identical for merging purposes which I implemented once while trying to
replicate Tiled's merging system but realised quickly that it does not allow for blending, just ignoring that another 
set is different. This would work perfectly here but I'll leave that for now.

2021-08-29
Making this less tightly coupled
- HitDetector broadcasts pos of tile it's currently over
- FarmManager receives this event and moves the TileHighlight when the appropriate tool is selected
  - Player broadcasts selected tool
- FarmManager handles interaction input
  - Handles deciding when to make a tile Soil
  - Tracks which tiles are soil for water level tracking

# Tools
Handling different tools really should be a state system but since I've only got two I'm just going to slap if 
statements everywhere and call it a day.

# Global Vars in Scripts
There's a few places, FarmManager.gd for example, where I have top level variables. This then causes me issues when I
want to use the same name later and really is just me being lazy and writing bad code. I should try to not do that
again.

# Resources
Apparently you should store data in a Resource instead of a Node as they're lighter.
https://www.reddit.com/r/godot/comments/pevp3a/game_architecture_and_abstract_objects_how_to/

# Art Change
2021-09-08
Changing art from LPC to Cozy because I realised that the farmable area only needs to be a tiny chunk of the map. That
left a lot of empty space which I didn't have assets for, but the Cozy Farm pack has everything I need as well as houses
and other decorations that can go in the empty space.

This has been interesting as LPC is 32px and Cozy is 16px. I initially doubled the ground asset size in Krita but then
saw a comment saying I could just scale the TileSet in Godot. Reset it back to the 16px assets and tried that but had
lines between each tile. Solution to that was to reimport the image but with `filter` deselected. Now it looks perfect.

I also realised you can layer autotile regions in Godot, you just need to use the arrows to switch between them. So long
as they're named sanely it's not that confusing. I now have the Spring tiles set up with good names and icons.

# Moving Farmable to a Layer
Originally all of the ground was on one layer and farmable tiles were identified by the name "Farmable" which was a
clone of the Grass tile. I've realised that I can make things more flexible by having multiple layers and making one
layer the Farmable layer. Anything on this layer can be turned into dirt and farmed. Means some code changes but it
means that combining tilesets can be done by overlapping them instead of filling the edges in manually.

My initial reluctance to use multiple tilesets was due to the NPC pathing issues it caused me in Gunmageddon but I'm
not planning on having NPCs walk in this game so I don't need to worry about that. If I DID need to handle that, I don't
know what I'd do. I keep forgetting that I ended up having to merge layers with code in Gunmageddon actually, so 
probably just that again.

# NPC reputation
2021-10-11
This is ultimately incrementing and decrementing a counter. Normally I'd want this to be stored on a player model but in
Carrots the only place this will get modified is inside Dialogic. Storing rep as a Dialogic Definition will make it
easier to modify. The model route would require the same work in Dialogic but with syncing the values to the models at
the end of each conversation so nothing is lost by not doing that and I can do it later if I see a need.

The result of the rep system will be NPCs guarding the carrots. This will happen when their rep hits a threshold and
would required either 1) sending a signal when it goes over ten, or 2) just checking it at the end of a conversation.
Which is like 50% of the sync work above. Sending a signal means logic in conversations which is annoying and not their
responsibility so just checking after each conversation ends is far easier.

Ended up syncing anyway because having people become guards will have other triggers as well as rep so I can't just make
that happen at the end of a conversation. I've also realised that I also need to prevent repeating a conversation from 
incrementing rep more than once. I really don't want to have to track that with vars manually.

Solution to repeating: track which timelines are played, toggle a var in Dialogic based on whether the timeline has been
played before, wrap rep changes in an if that checks if `scene_played` is 0 or 1.

# Walking Behind Multi-tile Sprites
For individual game objects like the player and NPCs, their z-index is set to their Y position. This ensures that the
player will correctly appear in front of or behind NPCs as they move around.

This needs to be done to things like the lamppost but can't be because you can't set the z-index of individual tiles,
only on the TileSet node itself.

Solved this by moving multi-tile objects into their own TileMaps and setting the z-index on ready.
